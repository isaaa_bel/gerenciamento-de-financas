# Gerenciamento Financeiro
Software utilizado para o gerenciamento financeiro da escola, ou seja, auxiliar no levantamento, gastos totais e outros.  

## Funcionalidades
    *Facilitar os cálculos financeiros  
    *Auxiliar na organização dos dados financeiros 
    *Auxiliar na pesquisa de gastos totais e levantamentos da escola.
