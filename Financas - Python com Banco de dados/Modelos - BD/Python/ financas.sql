DROP SCHEMA IF EXISTS financas;
CREATE SCHEMA IF NOT EXISTS financas;

CREATE TABLE financas.levantamento(
	id INT PRIMARY KEY AUTO_INCREMENT,
	vEstipulado FLOAT(5) NOT NULL,
	vContratado FLOAT(5) NOT NULL,
	vTotal FLOAT(5) NOT NULL
);

CREATE TABLE financas.cagastos(
	id INT PRIMARY KEY AUTO_INCREMENT,
	quantProd FLOAT(3) NOT NULL,
	vUnitario FLOAT(3) NOT NULL,
	vTotal FLOAT(3) NOT NULL
);

CREATE TABLE financas.senhas(
	id INT PRIMARY KEY AUTO_INCREMENT,
	senha VARCHAR(50) NOT NULL
);

CREATE TABLE financas.usuario(
	login VARCHAR(50) PRIMARY KEY,
	senha INT(5) NOT NULL UNIQUE,
	foreign key(senha) references senhas(id)
);
