import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
from Visual.Home.Home import Home
from Visual.Css import Css
from Controle.LogicaControle import LogicaControle
from Visual.Cadastro.Cadastro import Cadastro
from Visual.Erro.Erro import Erro 
from Visual.Erro.ErroCadastro import ErroCadastro
from Visual.Erro.ErroInexistente import ErroInexistente
from Visual.Erro.ErroSI import ErroSI
from Controle.UsuarioControle import UsuarioControle
from Modelo.Login import Login
from Modelo.Senha import Senha
class LoginV:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("Visual/Login/Login.glade")
        builder.connect_signals(self)
        self.janela= builder.get_object("janela")
        self.label = builder.get_object("label")
        self.label1 = builder.get_object("label1")
        self.e = builder.get_object("e")
        self.e1 = builder.get_object("e1")
        self.erroS = builder.get_object("erroS")
        self.janela.show_all()
        self.janela.connect("destroy",Gtk.main_quit)
        a = Css()
        a.css()
        Gtk.main()
    #ir para tela principal - Home    
    def entrar(self,evt):
        try:
            usu=Login()
            selecionar = UsuarioControle()
            user = Login()
            user.setLogin(self.e.get_text())
            usu = selecionar.selecionarUmUser(user)
            if usu != "":
                s = Senha() 
                s.setSenha(self.e1.get_text())
                b=selecionar.selecionarUmSenha(usu)
                if b!="" and usu!="":
                    if selecionar.comparar(b,s):
                        self.janela.hide()
                        h = Home()
                        self.janela.show_all()
                    else:
                        es= ErroSI()        
                else:
                    e=Erro()
            elif self.e.get_text() == "" or self.e1.get_text() == "":
                er = Erro()
            else:
                e = ErroInexistente()
        except Exception as e:
            print(e)
        finally:
            self.e.set_text("")
            self.e1.set_text("")
            
    def saiir(self,evt):
        self.erro.hide()
        self.erroS.hide()
    #Rodizio de cadastro e login
    
    def irCadastro(self,evt):
        self.janela.hide()
        self.e.set_text("")
        self.e1.set_text("")
        b = Cadastro()
        self.janela.show_all()
        
            
    
        