import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
from Modelo.Itens import Itens
from Controle.LogicaControle import LogicaControle
from Controle.Calculos import Calculos
from Visual.Erro.Erro import Erro
from Visual.Erro.Erro2 import Erro2
from Visual.Erro.ErroIdNE import ErroIdNE
from Controle.CalcControle import CalcControle
from Visual.Levantamento.TelaId import TelaId
from Visual.Abre.Abre import Abre
from Visual.Abre.Abre2 import Abre2
class CalculoItens:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("Visual/CalculoItens/CItens.glade")
        builder.connect_signals(self)
        self.itens = builder.get_object("itens")
        self.lTotal = builder.get_object("lTotal")
        self.v1 = builder.get_object("v1")
        self.v2 = builder.get_object ("v2")
        self.pesEntry =  builder.get_object("pesEntry")
        self.itens.show_all()
        self.itens.connect("destroy",Gtk.main_quit)
        Gtk.main()
        
    def valorTotal(self,evt):
        ve = LogicaControle()
        if ve.verificarEmBranco(self.v1.get_text(),self.v2.get_text()):
            try:
                mod = Itens()
                mod.setQuantProd(self.v1.get_text())
                mod.setVUnitario(self.v2.get_text())
                m = Calculos()
                self.lTotal.set_text(str(m.vt(float(self.v1.get_text()),float(self.v2.get_text()))))
                mod.setVTotall(self.lTotal.get_text())
                instancia = CalcControle()
                instancia.inserirItens(mod)
                id=instancia.selecionarIdC()
                self.senha=TelaId(id)
            except Exception as e:
                e = Erro2()
            finally:
                self.v1.set_text("")
                self.v2.set_text("")
        else:
            ee = Erro()
            
    def pesquisa(self,evt):
            oi = LogicaControle()
            oie = CalcControle()
            if oi.abrePes(self.pesEntry):
                try:
                    interado = oie.selecionarUmC(int(self.pesEntry.get_text()))
                    self.pesEntry.set_text("")
                    if interado=="":
                        er = ErroIdNE()
                    else:
                        a = Abre2(interado)
                except Exception as e:
                    e = Erro2() 
            else:
                e = Erro()
            
    def sair(self,evt):
        self.itens.hide()
        Gtk.main_quit()
        
