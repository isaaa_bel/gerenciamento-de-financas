import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
class TelaId:
    def __init__(self,id):
        builder = Gtk.Builder()
        builder.add_from_file("Visual/CalculoItens/TelaId.glade")
        builder.connect_signals(self)
        self.senha = builder.get_object("senha")
        self.lSenha =  builder.get_object("lSenha")
        self.lSenha.set_text(str(id))
        self.senha.show_all()
        self.senha.connect("destroy",Gtk.main_quit)
        Gtk.main()
        