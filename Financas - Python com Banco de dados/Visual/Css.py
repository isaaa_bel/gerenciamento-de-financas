import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk,Gdk
class Css:
    def css(self):
        css = b"""
        .input{
            border-color: #000;
            background-color: #fff;
            color: #000;
        }
        .fundo{
            background-color: #fff;
        }
        .borda{
            border-color: #000;
            border-radius: 50px;
        }
        .blevantamento{
            color: #000;
            background-color: #42f4a1;
            border-radius: 50px;
            border-color: #356705;
        }
        .calculo{
            border-color: #000;
            border-radius: 50px;
            color: #000;
        }
        .calcular{
            border-radius: 50px;
            border-color: #f56;
        }
        
        .titulo{
            background-color: #000;
        }
        
        """
        style_provider = Gtk.CssProvider()
        style_provider.load_from_data(css) 
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            style_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            )
