import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
from Visual.Css import Css
class ErroS:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("Visual/Erro/ErroCadastro.glade")
        builder.connect_signals(self)
        self.sair = builder.get_object("sdiferente")
        self.sair.show_all()
        self.sair.connect("destroy",Gtk.main_quit)
        Gtk.main()
    def sair(self,evt):
        self.sair.hide()
    def voltar(self,evt):
        self.errocadastro.hide()