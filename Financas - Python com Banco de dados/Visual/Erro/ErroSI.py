import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
from Visual.Css import Css
class ErroSI:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("Visual/Erro/ErroCadastro.glade")
        builder.connect_signals(self)
        self.calma = builder.get_object("erroS")
        self.calma.show_all()
        self.calma.connect("destroy",Gtk.main_quit)
        Gtk.main()
    def sair(self,evt):
        self.calma.hide()
    def voltar(self,evt):
        self.errocadastro.hide()