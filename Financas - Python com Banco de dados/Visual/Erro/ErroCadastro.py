import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
from Visual.Css import Css
class ErroCadastro:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("Visual/Erro/ErroCadastro.glade")
        builder.connect_signals(self)
        self.errocadastro = builder.get_object("errocadastro")
        self.erroS = builder.get_object("erroS")
        self.sair = builder.get_object("sair")
        self.errocadastro.show_all()
        self.errocadastro.connect("destroy",Gtk.main_quit)
        Gtk.main()
