import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
from Visual.Css import Css
class Erro2:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("Visual/Erro/erroLetraN.glade")
        builder.connect_signals(self)
        self.erro2 = builder.get_object("errinho2")
        self.erro2.show_all()
        self.erro2.connect("destroy",Gtk.main_quit)
        Gtk.main()
