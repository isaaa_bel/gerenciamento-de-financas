import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
from Visual.Css import Css
class Erro:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("Visual/Erro/erroEmBranco.glade")
        builder.connect_signals(self)
        self.erro1 = builder.get_object("errinho")
        self.erro1.show_all()
        self.erro1.connect("destroy",Gtk.main_quit)
        Gtk.main()
