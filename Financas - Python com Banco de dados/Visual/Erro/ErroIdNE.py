import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
from Visual.Css import Css
class ErroIdNE:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("Visual/Erro/erroIdNE.glade")
        builder.connect_signals(self)
        self.erro = builder.get_object("err")
        self.erro.show_all()
        self.erro.connect("destroy",Gtk.main_quit)
        Gtk.main()
