import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
from Visual.Css import Css
class ErroInexistente:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("Visual/Erro/ErroCadastro.glade")
        builder.connect_signals(self)
        self.errocadastro = builder.get_object("erroInexistente")
        self.erroInexistente = builder.get_object("erroInexistente")
        self.erroInexistente = builder.get_object("erroInexistente")
        self.erroInexistente.show_all()
        self.erroInexistente.connect("destroy",Gtk.main_quit)
        Gtk.main()