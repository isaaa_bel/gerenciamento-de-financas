import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
from Modelo.LevantamentoM import LevantamentoM
from Controle.LogicaControle import LogicaControle
from Controle.Calculos import Calculos
from Visual.Erro.Erro import Erro
from Visual.Erro.Erro2 import Erro2
from Visual.Erro.ErroIdNE import ErroIdNE
from Visual.CalculoItens.CalculoItens import CalculoItens
from Controle.CalcControle import CalcControle
from Visual.Abre.Abre import Abre
from Visual.Levantamento.TelaId import TelaId
class Levantamento:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("Visual/Levantamento/Levantamento.glade")
        builder.connect_signals(self)
        self.levante = builder.get_object("levante")
        self.entVE = builder.get_object("entVE")
        self.entVC =  builder.get_object("entVC")
        self.label =  builder.get_object("label")
        self.pesEntry =  builder.get_object("pesEntry")
        self.levante.show_all()
        self.levante.connect("destroy",Gtk.main_quit)
        Gtk.main()
    
    def resultadoLevantamento(self,evt):
        ve = LogicaControle()
        if ve.verificarEmBranco(self.entVE.get_text(),self.entVC.get_text()):
            try:
                mod = LevantamentoM()
                mod.setVEstipulado(self.entVE.get_text())
                mod.setVContratado(self.entVC.get_text())
                e = Calculos()
                self.label.set_text(str(e.diferenca(float(self.entVE.get_text()),float(self.entVC.get_text()))))
                mod.setVTotal(self.label.get_text())
                instancia = CalcControle()
                instancia.inserirLevantamento(mod)
                id=instancia.selecionarId()
                self.senha=TelaId(id)
            except Exception as e:
                e = Erro2()
            finally:
                self.entVE.set_text("")
                self.entVC.set_text("")
        else:
            ee= Erro()
        
            
    #Barra de pesquisa
    def pesquisa(self,evt):
            oi = LogicaControle()
            oie = CalcControle()
            if oi.abrePes(self.pesEntry):
                try:
                    interado = oie.selecionarUm(int(self.pesEntry.get_text()))
                    self.pesEntry.set_text("")
                    if interado=="":
                        er = ErroIdNE()
                    else:
                        a = Abre(interado)
                except Exception as e:
                    e = Erro2() 
            else:
                e = Erro()
    def irCalculoItens(self,evt):
        self.levante.hide()
        a = CalculoItens()
        
    def volte(self,evt):
        self.levante.hide()
        Gtk.main_quit()
    
