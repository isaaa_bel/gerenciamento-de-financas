import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
class InsDoLev:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("Visual/Instru/Ins.glade")
        builder.connect_signals(self)
        self.insDoLev = builder.get_object("insDoLev")
        self.insDoLev.show_all()
        self.insDoLev.connect("destroy",Gtk.main_quit)
        Gtk.main()
