import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
class Abre2:
    def __init__(self,interado):
        builder = Gtk.Builder()
        builder.add_from_file("Visual/Abre/Abre.glade")
        builder.connect_signals(self)
        self.abre = builder.get_object("abre2")
        self.l = builder.get_object("l1")
        self.ll = builder.get_object("ll1")
        self.lll = builder.get_object("lll1")
        self.l.set_text(interado.getQuantProd())
        self.ll.set_text(interado.getVUnitario())
        self.lll.set_text(interado.getVTotall())
        self.abre.show_all()
        self.abre.connect("destroy",Gtk.main_quit)
        Gtk.main()
        