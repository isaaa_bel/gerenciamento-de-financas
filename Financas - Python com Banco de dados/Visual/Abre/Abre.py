import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
class Abre:
    def __init__(self,interado):
        builder = Gtk.Builder()
        builder.add_from_file("Visual/Abre/Abre.glade")
        builder.connect_signals(self)
        self.abre = builder.get_object("abre")
        self.l = builder.get_object("l")
        self.ll = builder.get_object("ll")
        self.lll = builder.get_object("lll")
        self.l.set_text(interado.getVEstipulado())
        self.ll.set_text(interado.getVContratado())
        self.lll.set_text(interado.getVTotal())
        self.abre.show_all()
        self.abre.connect("destroy",Gtk.main_quit)
        Gtk.main()
        