import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
from Visual.Css import Css
from Controle.LogicaControle import LogicaControle
from Controle.UsuarioControle import UsuarioControle
from Modelo.Login import Login
from Modelo.Senha import Senha
from Visual.Erro.Erro import Erro
from Visual.Erro.ErroS import ErroS
from Visual.Erro.ErroCadastro import ErroCadastro
class Cadastro:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("Visual/Cadastro/Cadastro.glade")
        builder.connect_signals(self)
        self.jandastro= builder.get_object("jandastro")
        self.ok = builder.get_object("ok")
        self.e = builder.get_object("e")
        self.e1= builder.get_object("e1")
        self.e2= builder.get_object("e2")
        self.jandastro.show_all()
        self.jandastro.connect("destroy",Gtk.main_quit)
        a = Css()
        a.css()
        Gtk.main()
        
    def irLogin(self,evt):
        try:
            self.janela.hide()
            h = Home()
        except Exception as e:
            print(e)
            
    def cadastrar(self,evt):
        try:
            inserir = UsuarioControle()
            modelo = Login()
            modelo.setLogin(self.e.get_text())
            model = Senha()
            modela=Senha()
            modela.setSenha(self.e2.get_text())
            model.setSenha(self.e1.get_text())
            if self.e.get_text() != "" and self.e1.get_text() != "" and self.e2.get_text() != "":
                if inserir.comparar(model,modela):
                    if inserir.inserirS(model):
                        model.setId(inserir.selecionarId())
                        modelo.setSenha(model.getId())
                        if inserir.inserir(modelo):   
                            self.ok.show_all() 
                        else:
                            #erro referente ao usuario
                            er = ErroCadastro()
                            
                    else:
                        #Erro referente a senhas
                        er = ErroCadastro()
                else:
                    #Erro de senhas diferentes
                    s = ErroS()
            else:
                e = Erro()
                    
        except Exception:
            pass
        finally:
            self.e.set_text("")
            self.e1.set_text("")
            self.e2.set_text("")
            
    # para quando o usuario apertar em ok, fechar a dialog    
    def ok(self,evt):
        self.ok.hide()
        
    # rodizio entre cadastro e login
    def voltar(self,evt):
        self.jandastro.hide()
        Gtk.main_quit()