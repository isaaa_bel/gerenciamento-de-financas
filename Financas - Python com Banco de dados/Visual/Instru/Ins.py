import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
class Ins:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("Visual/Instru/Ins.glade")
        builder.connect_signals(self)
        self.ins = builder.get_object("instrucoes")
        self.ins.show_all()
        self.ins.connect("destroy",Gtk.main_quit)
        Gtk.main()
    def voltar(self,evt):
        self.ins.hide()
        Gtk.main_quit()
