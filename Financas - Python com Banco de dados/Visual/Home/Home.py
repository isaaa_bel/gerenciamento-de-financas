import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
from Visual.Css import Css
from Visual.Instru.Ins import Ins
from Visual.Levantamento.Levantamento import Levantamento
from Visual.CalculoItens.CalculoItens import CalculoItens
class Home:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("Visual/Home/Home.glade")
        builder.connect_signals(self)
        self.janela = builder.get_object("principal")
        self.janela.show_all()
        self.janela.connect("destroy",Gtk.main_quit)
        a = Css()
        a.css()
        Gtk.main()
    def irLevant(self,evt):
        self.janela.hide()
        ir = Levantamento()
        self.janela.show_all()
    def inst(self,evt):
        self.janela.hide()
        i = Ins()
        self.janela.show_all()

    def irCalculoItens(self,evt):
        self.janela.hide()
        a = CalculoItens()
        self.janela.show_all()
        
    def fechar(self,evt):
        self.janela.hide()
        Gtk.main_quit()
        
        
