from Modelo.LevantamentoM import LevantamentoM
from Modelo.Itens import Itens
from Controle.Conexao import Conexao
#Classe exclusivamente para conexÃ£o com o banco
class CalcControle:
        def inserirLevantamento(self,valores):
            try:    
                conexao = Conexao()
            #retorna true quando estÃ¡ ativa
                sql="INSERT INTO levantamento(vEstipulado,vContratado,vTotal) VALUES(%s,%s,%s);"
                cursor = conexao.getCon().cursor()
                valores = (float(valores.getVEstipulado()),float(valores.getVContratado()),float(valores.getVTotal()))
                cursor.execute(sql,valores)
            #mysqlconnector do python segue os mesmos princios da consulta do banco de dados
                conexao.getCon().commit()
                conexao.fecharConexao()
                return True
            except Exception as e:
                print("Erro: "+str(e))
                
        def inserirItens(self,valores):
            try:    
                #mesmo principio de inserirLevantamento
                conexao = Conexao()
                sql="INSERT INTO cagastos(quantProd,vUnitario,vTotal) VALUES(%s,%s,%s);"
                cursor = conexao.getCon().cursor()
                valores = (float(valores.getQuantProd()),float(valores.getVUnitario()),float(valores.getVTotall()))
                cursor.execute(sql,valores)
                conexao.getCon().commit()
                conexao.fecharConexao()
                return True
            except Exception as e:
                print("Erro: "+str(e))
                
        #MÃ©todos para selecionar o id
        #levantamento  
        def selecionarUm(self,id):
            try:
                conexao = Conexao()
                sql= "SELECT * FROM levantamento WHERE id=%s;"
                cursor = conexao.getCon().cursor(dictionary=True)
                cursor.execute(sql,(id,))
                consulta = cursor.fetchone()
                levanta = LevantamentoM()
                levanta.setVEstipulado(str(consulta['vEstipulado']))
                levanta.setVContratado(str(consulta['vContratado']))
                levanta.setVTotal(str(consulta['vTotal']))
                return levanta
            except Exception as e:
                return ""
        #cagastos
        def selecionarUmC(self,id):
            try:
                conexao = Conexao()
                sql= "SELECT * FROM cagastos WHERE id=%s;"
                cursor = conexao.getCon().cursor(dictionary=True)
                cursor.execute(sql,(id,))
                consulta = cursor.fetchone()
                cagastos = Itens()
                cagastos.setQuantProd(str(consulta['quantProd']))
                cagastos.setVUnitario(str(consulta['vUnitario']))
                cagastos.setVTotall(str(consulta['vTotal']))
                return cagastos
            except Exception as e:
                return ""
        
        def selecionarId(self):
            conexao = Conexao()
            levanta = LevantamentoM()
            sql= "SELECT * FROM levantamento;"
            cursor = conexao.getCon().cursor(dictionary=True)
            cursor.execute(sql)
            consulta = cursor.fetchall()
            for i in range(0,consulta.__len__(),1):
                levanta.setId(consulta[i]['id'])
            return levanta.getId() 
        
        def selecionarIdC(self):
            try:
                conexao = Conexao()
                cagastos = Itens()
                sql= "SELECT * FROM cagastos;"
                cursor = conexao.getCon().cursor(dictionary=True)
                cursor.execute(sql)
                consulta = cursor.fetchall()
                for i in range(0,consulta.__len__(),1):
                    cagastos.setId(consulta[i]['id'])
                return cagastos.getId()
            except Exception as e:
                print(e) 
            