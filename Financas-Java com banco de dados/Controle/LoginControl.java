package Controle;
import Modelo.Senha;
import Modelo.CadastroMod;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class LoginControl {
    public CadastroMod selecionarUser(CadastroMod log){
        CadastroMod a=new CadastroMod();
        try{
            Conexao con = new Conexao();
            PreparedStatement lo=con.getConexao().prepareStatement("SELECT * FROM usuario WHERE login=?;");
            lo.setString(1,log.getUsuario());
            if(lo.execute()){
                ResultSet rs=lo.executeQuery();
                if(rs!=null){
                    if(rs.next()){
                        a.setUsuario(rs.getString(("login")));
                        a.setSenha(rs.getInt("senha"));
                    }
                }
                con.fecharConexao();
            }else{
                con.fecharConexao();
            }
            con.fecharConexao();
        }catch(Exception e){
            System.out.println("Erro: "+ e.getMessage());
            a=null;
        }
        return a;
    }
    public Senha selecionarSenha(CadastroMod sen){
        Senha senha=new Senha();
        try{
            Conexao con=new Conexao();
            PreparedStatement se=con.getConexao().prepareStatement("SELECT * FROM senhas WHERE id=?;");
            se.setInt(1,sen.getSenha());
            if(se.execute()){
                ResultSet rs=se.executeQuery();
                if(rs!=null){
                    if(rs.next()){
                        senha.setId(rs.getInt("id"));
                        senha.setSenha(rs.getString("senha"));
                    }
                }
            }
            con.fecharConexao();
        }catch(Exception e){
            System.out.println("Erro besta: "+e.getMessage());
            senha=null;
        }
        return senha;
    }
}
