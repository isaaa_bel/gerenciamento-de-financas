package Controle;

import javax.swing.JOptionPane;
import Modelo.CalcMod;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
/**
 *
 * @author isaaa
 */
public class CalcControl {
        // CRUD 
        public ArrayList<CalcMod> selecionarAll(){
            ArrayList<CalcMod> calc=new ArrayList();
            try{
                Conexao con=new Conexao();
                PreparedStatement calcula=con.getConexao().prepareStatement("SELECT * FROM cagastos;");                
                    ResultSet rs=calcula.executeQuery();
                    while(rs.next()){
                        CalcMod cal=new CalcMod();
                        cal.setNI(rs.getString("nI"));
                        cal.setCoditem(rs.getInt("codigoI"));                        
                        cal.setVP(rs.getFloat("quantProd"));
                        cal.setVU(rs.getFloat("vUnitario"));
                        cal.setResult(rs.getFloat("vTotal"));
                        calc.add(cal);
                    }                
                con.fecharConexao();
            }catch(Exception e){
                System.out.println("Erro: "+e.getMessage());
            }
            return calc;
        }
        // MÃ©todo de inserÃ§Ã£o no banco
        public boolean calcGastos(CalcMod gastinho){
        boolean total;
        try{
            Conexao con = new Conexao();
            PreparedStatement calcular = con.getConexao().prepareStatement("INSERT INTO cagastos(nI,codigoI,quantProd,vUnitario,vTotal) VALUES(?,?,?,?,?);");
            calcular.setString(1,gastinho.getNI());
            calcular.setInt(2,gastinho.getCoditem());
            calcular.setFloat(3,gastinho.getVP());
            calcular.setFloat(4,gastinho.getVU());
            calcular.setFloat(5,gastinho.getResult());
            if(!calcular.execute()){
                con.fecharConexao();
                total = true;
            }else{
                con.fecharConexao();
                total = false;
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,"O VIPROC nÃ£o pode ser o mesmo, esses valores nÃ£o serÃ£o armazenados.","Aviso",JOptionPane.ERROR_MESSAGE);        
            total = false;
        }
            return total;
        }
        // atualizar dados da tabela no banco
        public void atualizarValores(CalcMod calc){
            try{
                Conexao con=new Conexao();
                PreparedStatement calcular=con.getConexao().prepareStatement("UPDATE cagastos SET nI=?,codigoI=?,quantProd=?,vUnitario=?,vTotal=? WHERE codigoI=?;");
                calcular.setString(1,calc.getNI());   
                calcular.setInt(2,calc.getCoditem());                
                calcular.setFloat(3,calc.getVP());
                calcular.setFloat(4,calc.getVU());
                calcular.setFloat(5, calc.getResult());   
                calcular.setInt(6,calc.getCoditem());
                calcular.execute();
                con.fecharConexao();
            }catch(Exception e){
                System.out.println("Erro bonitinho: "+e.getMessage());
            }
        }
        
        
         //remover dados da tabela no banco
        public boolean remover(CalcMod cadastro){
        boolean resul;
        try{
            Conexao con = new Conexao();
            PreparedStatement calcular=con.getConexao().prepareStatement("DELETE FROM cagastos WHERE codigoI=?");
            calcular.setInt(1,cadastro.getCoditem());
            if(!calcular.execute()){
                con.fecharConexao();
                resul=true;
            }else{
                con.fecharConexao();
                resul=false;
            }
        }catch(Exception e){
            System.out.println("Erro: "+ e.getMessage());
            resul=false;
        }
        return resul;
    }
}
