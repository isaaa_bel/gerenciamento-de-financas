package Controle;

import Modelo.LevantaMod;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Isaaa
 */

public class LevControl {
    //MÃ©todo de Selecionar Todos, para ficarem salvos na tabela
        public ArrayList<LevantaMod> selecionarAll(){
            ArrayList<LevantaMod> leves=new ArrayList();
            try{
                Conexao con=new Conexao();
                PreparedStatement levanta=con.getConexao().prepareStatement("SELECT * FROM levantamento;");                
                    ResultSet rs=levanta.executeQuery();
                    while(rs.next()){
                        LevantaMod leve=new LevantaMod();
                        leve.setNomeO(rs.getString("nomeO"));
                        leve.setEntradaSSA(rs.getString("entradaSSA"));
                        leve.setVIPROC(rs.getInt("VIPROC"));
                        leve.setPlanejamentoAnual(rs.getString("pAnual"));
                        leve.setVEstipulado(rs.getFloat("vEstipulado"));
                        leve.setVContratado(rs.getFloat("vContratado"));
                        leve.setVTotal(rs.getFloat("vTotal"));
                        leves.add(leve);                                        
                    }
                    con.fecharConexao();
            }catch(Exception e){
                System.out.println("Erro: "+e.getMessage());
            }
            return leves;
        }
    //inserir dados da tabela no banco
        public boolean inserirValores(LevantaMod leve){
        boolean resultado;
        try{
            Conexao con=new Conexao();
            PreparedStatement levanta=con.getConexao().prepareStatement("INSERT INTO levantamento(nomeO,entradaSSA,VIPROC,pAnual,vEstipulado,vContratado,vTotal) VALUES(?,?,?,?,?,?,?);");
            levanta.setString(1,leve.getNomeO());
            levanta.setString(2,leve.getEntradaSSA());
            levanta.setInt(3,leve.getVIPROC());
            levanta.setString(4, leve.getPlanejamentoAnual());
            levanta.setFloat(5,leve.getVEstipulado());
            levanta.setFloat(6,leve.getVContratado());
            levanta.setFloat(7, leve.getVTotal());
            if(!levanta.execute()){
                con.fecharConexao();
                resultado=true;
            }else{
                con.fecharConexao();
                resultado=false;
            }
        }catch(Exception e){
                JOptionPane.showMessageDialog(null,"O VIPROC nÃ£o pode ser o mesmo, esses valores nÃ£o serÃ£o armazenados.","Aviso",JOptionPane.ERROR_MESSAGE);        
            resultado=false;
        }
        return resultado;
        }
        //Atualizar dados da tabela no banco
        public void atualizarValores(LevantaMod leve){
            try{
                Conexao con=new Conexao();
                PreparedStatement levanta=con.getConexao().prepareStatement("UPDATE levantamento SET nomeO=?,VIPROC=?,entradaSSA=?,pAnual=?,vEstipulado=?,vContratado=?,vTotal=? WHERE VIPROC=?;");
                levanta.setString(1,leve.getNomeO());
                levanta.setInt(2,leve.getVIPROC());
                levanta.setString(3,leve.getEntradaSSA());
                levanta.setString(4, leve.getPlanejamentoAnual());
                levanta.setFloat(5,leve.getVEstipulado());
                levanta.setFloat(6,leve.getVContratado());
                levanta.setFloat(7, leve.getVTotal());
                levanta.setInt(8,leve.getVIPROC());
                levanta.execute();
                con.fecharConexao();
            }catch(Exception e){
                System.out.println("Erro bonitinho: "+e.getMessage());
            }
        }
        //remover dados da tabela no banco
        public boolean remover(LevantaMod leve){
        boolean resul;
        try{
            Conexao con = new Conexao();
            PreparedStatement leva=con.getConexao().prepareStatement("DELETE FROM levantamento WHERE VIPROC=?");
            leva.setInt(1,leve.getVIPROC());
            if(!leva.execute()){
                con.fecharConexao();
                resul=true;
            }else{
                con.fecharConexao();
                resul=false;
            }
        }catch(Exception e){
            System.out.println("Erro: "+ e.getMessage());
            resul=false;
        }
        return resul;
    }
}
