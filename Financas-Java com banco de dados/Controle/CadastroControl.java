package Controle;
import Modelo.CadastroMod;
import Modelo.Senha;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CadastroControl {
    // Cadastro do Usuario
    public boolean Cadastrar(CadastroMod cadinho){
        boolean cof;
        try{
            Conexao con = new Conexao();
            PreparedStatement cad=con.getConexao().prepareStatement("INSERT INTO usuario(login,senha) VALUES(?,?);");
            cad.setString(1,cadinho.getUsuario());
            cad.setInt(2,cadinho.getSenha());
            if(!cad.execute()){
                con.fecharConexao();
                cof=true;
            }else{
                con.fecharConexao();
                cof=false;
            }
        }catch(Exception e){
            System.out.println("Erro: "+ e.getMessage());
            cof=false;
        }
        return cof;
    }
    //senha
    public boolean Senha(Senha sn){
        boolean senha;
        try{
            Conexao con = new Conexao();
            PreparedStatement sen=con.getConexao().prepareStatement("INSERT INTO senhas(senha) VALUES(?);");
            sen.setString(1,sn.getSenha());
            if(!sen.execute()){
                con.fecharConexao();
                senha=true;
            }else{
                con.fecharConexao();
                senha=false;
            }
        }catch(Exception e){
            System.out.println("Erro: "+ e.getMessage());
            senha=false;
        }
        return senha;
    }
    //Selecionar o Id para a senha
    public int SelecionarId(){
        int senha=0;
        try{
            Conexao con=new Conexao();
            PreparedStatement sen=con.getConexao().prepareStatement("SELECT * FROM senhas;");
            if(sen.execute()){
                ResultSet rs=sen.executeQuery();
                if(rs!=null){
                    while(rs.next()){
                        senha=rs.getInt("id");
                    }
                }
                con.fecharConexao();
            }
        }catch(Exception e){
            System.out.println("Erro geral: "+e.getMessage());
            return senha;
        }
        return senha; 
    }
}
