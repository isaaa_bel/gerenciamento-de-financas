package Modelo;

import javax.swing.JOptionPane;

/**@author Isaaa e cordeiro
 Atributos e mÃ©todos da janela de Calculo dos itens
 **/
public class CalcMod {
    private String nI;
    private int coditem;    
    private float VP;
    private float VU;
    private float Result;
    public String getNI(){
        return nI;
    }
    public void setNI(String ni){
        this.nI=ni;
    }
    public int getCoditem() {
        return coditem;
    }
    public void setCoditem(int coditem) {
        this.coditem = coditem;
    }   
    public float getVP() {
        return VP;
    }
    public void setVP(float VP) {
        this.VP = VP;
    }
    public float getVU() {
        return VU;
    }
    public void setVU(float VU) {
        this.VU = VU;
    }
    public float getResult() {
        return Result;
    }
    public void setResult(float Result) {
        this.Result = Result;
    }
    
}
