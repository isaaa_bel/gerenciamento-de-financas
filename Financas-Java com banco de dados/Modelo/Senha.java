package Modelo;
/**
 *
 * @author Isaaa e cordeiro
 * Atributos e mÃ©todos para serem utilizados na tabela senha
 */
public class Senha {
    private int id;
    private String senha;
    private String confSenha;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getSenha() {
        return senha;
    }
    public void setSenha(String senha) {
        this.senha = senha;
    }
     public String getConfSenha() {
        return confSenha;
    }
    public void setConfSenha(String confSenha) {
        this.confSenha = confSenha;
    }
}
 