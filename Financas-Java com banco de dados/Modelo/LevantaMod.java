package Modelo;

/**
 *
 * @author Isaaa e cordeiro
 * Atributos e mÃ©todos da janela de Levantamento
 */
public class LevantaMod {
    private String nomeO;
    private int VIPROC;
    private String entradaSSA;
    private String planejamentoAnual;
    private float vEstipulado;
    private float vContratado;
    private float vTotal; 
    public void setNomeO(String n){
        this.nomeO=n;
    }
    public String getNomeO(){
        return this.nomeO;
    }
    public void setVIPROC(int vi){
        this.VIPROC=vi;
    }
    public int getVIPROC(){
        return this.VIPROC;
    }
    public void setEntradaSSA(String enss){
        this.entradaSSA=enss;
    }
    public String getEntradaSSA(){
        return this.entradaSSA;
    }
    public void setPlanejamentoAnual(String pa){
        this.planejamentoAnual=pa;
    }
    public String getPlanejamentoAnual(){
        return this.planejamentoAnual;
    }
    public void setVEstipulado(float ve){
        this.vEstipulado=ve;
    }
    public float getVEstipulado(){
        return this.vEstipulado;
    }
    public void setVContratado(float vc){
        this.vContratado = vc;
    }
    public float getVContratado(){
        return this.vContratado;
    }
    public void setVTotal(float vt){
        this.vTotal=vt;
    }
    public float getVTotal(){
        return this.vTotal;
    }
}
