/**
 *@author cordeiro e Isaaa
 **/
package Visual;
import Controle.Calc;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import Controle.CalcControl;
import Modelo.CalcMod;
import java.util.ArrayList;

public class Calculo extends javax.swing.JFrame  {

    /*Construtor de calculos que executa o mÃ©todo Selecionar tudo e mantÃªm os dados salvos na tabela*/
    public Calculo() {
        initComponents();
        CalcControl con=new CalcControl();
        ArrayList<CalcMod> array=new ArrayList();        
        array=con.selecionarAll();            
        for(int a=0;a<array.size();a++){
            DefaultTableModel dtmProdutos;
            dtmProdutos = (DefaultTableModel) JTProdutos.getModel();        
            Object[] dados = {array.get(a).getNI(),array.get(a).getCoditem(),array.get(a).getVP(),array.get(a).getVU(),array.get(a).getResult()};
            dtmProdutos.addRow(dados);                                                
        }                                 
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jScrollBar1 = new javax.swing.JScrollBar();
        jScrollPane4 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        jCheckBox1 = new javax.swing.JCheckBox();
        jPanel1 = new javax.swing.JPanel();
        lblQuanProd = new javax.swing.JLabel();
        lblValUni = new javax.swing.JLabel();
        btnEnviar = new javax.swing.JButton();
        lblValTotal = new javax.swing.JLabel();
        btnLeva = new javax.swing.JButton();
        aviso = new javax.swing.JLabel();
        result = new javax.swing.JTextField();
        btnVm = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        JTProdutos = new javax.swing.JTable();
        jButton2 = new javax.swing.JButton();
        ano = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        codItem = new javax.swing.JTextField();
        VP = new javax.swing.JTextField();
        VU = new javax.swing.JTextField();
        jButton5 = new javax.swing.JButton();
        nI = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jButton1ActionPerformed = new javax.swing.JButton();
        jButton1ActionPerformed1 = new javax.swing.JButton();

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane4.setViewportView(jList1);

        jCheckBox1.setText("jCheckBox1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(254, 254, 254));

        lblQuanProd.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lblQuanProd.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblQuanProd.setText("Quantidade:");

        lblValUni.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lblValUni.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblValUni.setText("Valor unitÃ¡rio:");

        btnEnviar.setText("Calcular");
        btnEnviar.setBorder(null);
        btnEnviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnviarActionPerformed(evt);
            }
        });

        lblValTotal.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lblValTotal.setText("Valor total:");

        btnLeva.setText("Calcular Levantamento");
        btnLeva.setBorder(null);
        btnLeva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLevaActionPerformed(evt);
            }
        });

        aviso.setText("Obs: Para nÃºmeros nÃ£o inteiros, utilize um ponto ao invÃ©s da vÃ­rgula.");

        result.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resultActionPerformed(evt);
            }
        });

        btnVm.setText("Voltar ao menu");
        btnVm.setBorder(null);
        btnVm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVmActionPerformed(evt);
            }
        });

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/javaapplication1/Imagem/logo.png"))); // NOI18N

        jButton3.setText("Enviar dados");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        JTProdutos.setBackground(new java.awt.Color(153, 255, 255));
        JTProdutos.setForeground(new java.awt.Color(0, 0, 0));
        JTProdutos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome do Item", "CÃ³d. do Item", "Quant.", "Valor Unit.", "Valor Total"
            }
        ));
        JTProdutos.setGridColor(new java.awt.Color(255, 255, 255));
        JTProdutos.setSelectionBackground(new java.awt.Color(0, 51, 255));
        JTProdutos.setSelectionForeground(new java.awt.Color(255, 255, 255));
        JTProdutos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JTProdutosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(JTProdutos);

        jButton2.setText("Atualizar dados");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        ano.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N

        jPanel3.setBackground(new java.awt.Color(51, 255, 153));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 10, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jLabel5.setFont(new java.awt.Font("Dialog", 1, 15)); // NOI18N
        jLabel5.setText("Nome do Item:");

        jLabel6.setFont(new java.awt.Font("Dialog", 1, 15)); // NOI18N
        jLabel6.setText("CÃ³gido do Item:");

        codItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                codItemActionPerformed(evt);
            }
        });

        VP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VPActionPerformed(evt);
            }
        });

        VU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VUActionPerformed(evt);
            }
        });

        jButton5.setText("Limpar campos");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        nI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nIActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Dialog", 1, 30)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 204, 102));
        jLabel7.setText("Tabela de CÃ¡lculo dos Itens.");

        jButton1ActionPerformed.setText("Excluir dados");
        jButton1ActionPerformed.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformedActionPerformed(evt);
            }
        });

        jButton1ActionPerformed1.setText("Sair");
        jButton1ActionPerformed1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(aviso)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(btnLeva, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(31, 31, 31)
                                            .addComponent(jButton5)
                                            .addGap(28, 28, 28)
                                            .addComponent(jButton3)))))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(48, 48, 48)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(btnEnviar, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel6)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addGap(9, 9, 9)
                                            .addComponent(lblQuanProd))
                                        .addComponent(jLabel5)))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(91, 91, 91)
                                        .addComponent(lblValTotal)
                                        .addGap(26, 26, 26)
                                        .addComponent(result, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(VP, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(lblValUni)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(VU, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(codItem, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(nI, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                        .addComponent(ano)
                        .addGap(11, 11, 11))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnVm, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(183, 183, 183)))
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(611, 611, 611)
                        .addComponent(jButton2)
                        .addGap(37, 37, 37)
                        .addComponent(jButton1ActionPerformed)
                        .addGap(37, 37, 37)
                        .addComponent(jButton1ActionPerformed1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(398, 398, 398)
                        .addComponent(jLabel7))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1025, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(21, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(jLabel1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel2))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(ano, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(67, 67, 67)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(nI, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(codItem, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(66, 66, 66)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuanProd, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(VP, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblValUni, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(VU, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEnviar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblValTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(result, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(69, 69, 69)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnLeva, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton5)
                    .addComponent(jButton3))
                .addGap(64, 64, 64)
                .addComponent(btnVm, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(56, 56, 56)
                .addComponent(aviso)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 615, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(64, 64, 64)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1ActionPerformed)
                    .addComponent(jButton1ActionPerformed1)
                    .addComponent(jButton2))
                .addGap(123, 123, 123))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 821, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnLevaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLevaActionPerformed
        dispose();
        Levantamento levanta = new Levantamento();
        levanta.setVisible(true);
        levanta.setResizable(false);
    }//GEN-LAST:event_btnLevaActionPerformed

    private void btnEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnviarActionPerformed
       
        if(VP.getText().equals("") && VU.getText().equals("")){
            JOptionPane.showMessageDialog(null,"Parece que vocÃª esqueceu de digitar algum valor, verifique em um dos campos","Aviso",JOptionPane.WARNING_MESSAGE);
            return;
        }
       
        if(VP.getText().equals("")){
            JOptionPane.showMessageDialog(null,"Parece que vocÃª esqueceu de digitar algum valor, verifique no campo Quantidade do produto","Aviso",JOptionPane.WARNING_MESSAGE);
            return;
        }
        if(VU.getText().equals("")){
            JOptionPane.showMessageDialog(null,"Parece que vocÃª esqueceu de digitar algum valor, verifique no campo Valor UnitÃ¡rio","Aviso",JOptionPane.WARNING_MESSAGE);
            return;
        }
        
        
        try{
            Calc con=new Calc();
            this.result.setText(String.valueOf(con.calcularG(Float.parseFloat(this.VP.getText()),Float.parseFloat(this.VU.getText()))));
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,"          OPS, Algo deu errado\nVocÃª deve digitar somente nÃºmeros!","Aviso",JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnEnviarActionPerformed

    private void btnVmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVmActionPerformed
        dispose();
        jan menu = new jan();
        menu.setVisible(true);
        menu.setResizable(false);
    }//GEN-LAST:event_btnVmActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try{
            if(JTProdutos.getSelectedRow() != -1) { 
               CalcMod valores=new CalcMod();
               CalcControl cadastre=new CalcControl();
               valores.setNI(nI.getText());
               valores.setCoditem(Integer.parseInt(codItem.getText()));                      
               valores.setVP(Float.parseFloat(VP.getText()));
               valores.setVU(Float.parseFloat(VU.getText()));
               valores.setResult(Float.parseFloat(result.getText()));
               cadastre.atualizarValores(valores);          
               JTProdutos.setValueAt(nI.getText(),JTProdutos.getSelectedRow(),0); 
               JTProdutos.setValueAt(codItem.getText(),JTProdutos.getSelectedRow(),1);
               JTProdutos.setValueAt(VP.getText(),JTProdutos.getSelectedRow(),2);
               JTProdutos.setValueAt(VU.getText(), JTProdutos.getSelectedRow(),3);
               JTProdutos.setValueAt(result.getText(),JTProdutos.getSelectedRow(),4);

             }else{
                JOptionPane.showMessageDialog(null,"Selecione um produto para excluir.");
            }
        }catch(Exception e){
            System.out.print("Erro");
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        try{
            CalcControl calculo;
            calculo = new CalcControl();
            CalcMod valores = new CalcMod();
            valores.setNI(nI.getText());
            valores.setCoditem(Integer.parseInt(codItem.getText()));
            valores.setVP((int) Float.parseFloat(VP.getText()));
            valores.setVU((int) Float.parseFloat((VU.getText())));
            valores.setResult((int) Float.parseFloat(result.getText()));
            if(codItem.getText().equals("")){
                JOptionPane.showMessageDialog(null,"Parece que vocÃª esqueceu de digitar algum valor","Aviso",JOptionPane.WARNING_MESSAGE);
                return;
            }
           if(VP.getText().equals("")){
                JOptionPane.showMessageDialog(null,"Parece que vocÃª esqueceu de digitar algum valor","Aviso",JOptionPane.WARNING_MESSAGE);
                return;
            }
           if(VU.getText().equals("")){
                JOptionPane.showMessageDialog(null,"Parece que vocÃª esqueceu de digitar algum valor","Aviso",JOptionPane.WARNING_MESSAGE);
                return;
            }
           if(result.getText().equals("")){
                JOptionPane.showMessageDialog(null,"Parece que vocÃª esqueceu de digitar algum valor","Aviso",JOptionPane.WARNING_MESSAGE);
                return;
            }
            DefaultTableModel dtmProdutos;
            dtmProdutos = (DefaultTableModel) JTProdutos.getModel();
            Object[] dados = {nI.getText(),codItem.getText(),VP.getText(),VU.getText(),result.getText()};
            dtmProdutos.addRow(dados);       
            nI.setText("");
            codItem.setText("");
            VP.setText("");
            VU.setText("");
            result.setText("");
            calculo.calcGastos(valores);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,"VocÃª deve digitar somente nÃºmeros no CÃ³digo do Item e nos cÃ¡lculos.","Aviso",JOptionPane.WARNING_MESSAGE);
        }
        
    }//GEN-LAST:event_jButton3ActionPerformed

    private void JTProdutosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JTProdutosMouseClicked
        int i = JTProdutos.getSelectedRow();
         TableModel model = JTProdutos.getModel();
         nI.setText(model.getValueAt(i,0).toString());
         codItem.setText(model.getValueAt(i,1).toString());                           
         VP.setText(model.getValueAt(i,2).toString());
         VU.setText(model.getValueAt(i,3).toString());
         result.setText(model.getValueAt(i,4).toString());
        
    }//GEN-LAST:event_JTProdutosMouseClicked

    private void resultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resultActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_resultActionPerformed

    private void codItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_codItemActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_codItemActionPerformed

    private void VPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VPActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_VPActionPerformed

    private void VUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VUActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_VUActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        codItem.setText("");
        VP.setText("");
        VU.setText("");
        result.setText("");
    }//GEN-LAST:event_jButton5ActionPerformed

    private void nIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nIActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nIActionPerformed

    private void jButton1ActionPerformedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformedActionPerformed
        if(JTProdutos.getSelectedRow() != -1){
            CalcControl calcule= new CalcControl();
            CalcMod valor = new CalcMod();
            valor.setCoditem(Integer.parseInt(codItem.getText()));
            DefaultTableModel dtmProdutos = (DefaultTableModel) JTProdutos.getModel();
            dtmProdutos.removeRow(JTProdutos.getSelectedRow());
            nI.setText("");
            codItem.setText("");            
            VP.setText("");
            VU.setText("");
            result.setText("");
            calcule.remover(valor);
            
       }else{
        JOptionPane.showMessageDialog(null,"Selecione um produto para excluir.");
       }
    }//GEN-LAST:event_jButton1ActionPerformedActionPerformed

    private void jButton1ActionPerformed1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed1ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton1ActionPerformed1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Calculo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Calculo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Calculo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Calculo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new Calculo().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable JTProdutos;
    private javax.swing.JTextField VP;
    private javax.swing.JTextField VU;
    private javax.swing.JLabel ano;
    private javax.swing.JLabel aviso;
    private javax.swing.JButton btnEnviar;
    private javax.swing.JButton btnLeva;
    private javax.swing.JButton btnVm;
    private javax.swing.JTextField codItem;
    private javax.swing.JButton jButton1ActionPerformed;
    private javax.swing.JButton jButton1ActionPerformed1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton5;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JList<String> jList1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollBar jScrollBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JLabel lblQuanProd;
    private javax.swing.JLabel lblValTotal;
    private javax.swing.JLabel lblValUni;
    private javax.swing.JTextField nI;
    private javax.swing.JTextField result;
    // End of variables declaration//GEN-END:variables

    void setVsisible(boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
